var fs = require("fs")
var http = require("http")
var url = require("url")
var path = require("path")
var fPath = "./videos/movie.mp4"

http.createServer(function (req, res) {
   if (!req.url.startsWith("/videos/")) {
      res.writeHead(200, {"Content-Type": "text/html"})
      res.end("<video src='" + fPath  + "' controls></video>")
   } else {
      fs.stat(fPath, function (err, stats) {
         if (err) {
            if (err.code === "ENOENT") {
               res.writeHead(404)
            }
            res.end(err)
         } else {
            var range = req.headers.range
            if (!range) {
               res.writeHead(416)
               res.end()
            } else {
               var positions = range.replace(/bytes=/, "").split("-")
               var start = parseInt(positions[0], 10)
               var total = stats.size
               var end = positions[1] ? parseInt(positions[1], 10): total - 1
               var chunksize = (end - start) + 1
               var maxChunk = 1024 * 1024
               if (chunksize > maxChunk) {
                  end = start + maxChunk -1
                  chunksize = (end - start) + 1
               }

               res.writeHead(206, {
                  "Content-Range": "bytes " + start + "-" + end + "/" + total,
                  "Accept-Range": "bytes",
                  "Content-Length": chunksize,
                  "Content-Type": "video/mp4"
               })

               var stream = fs.createReadStream(fPath, {start: start, end: end, autoClose: true})
                  .on("open", function () {
                     stream.pipe(res)
                  })
                  .on("error", function (err) {
                     res.end(err)
                  })
            }
         }
      })
   }
}).listen(8080)


